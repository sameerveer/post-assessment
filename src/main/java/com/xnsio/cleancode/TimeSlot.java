package com.xnsio.cleancode;

import java.time.LocalDateTime;
import java.util.Objects;

public class TimeSlot implements Comparable<TimeSlot> {
    private final LocalDateTime startTime;
    private final LocalDateTime endTime;
    private final boolean available;
    private final String title;

    public TimeSlot(LocalDateTime startTime, LocalDateTime endTime, boolean available, String title) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.available = available;
        this.title = title;
    }

    public boolean isAvailable() {
        return available;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeSlot that = (TimeSlot) o;
        return available == that.available &&
                Objects.equals(startTime, that.startTime) &&
                Objects.equals(endTime, that.endTime) &&
                Objects.equals(title, that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startTime, endTime, available, title);
    }

    @Override
    public int compareTo(TimeSlot timeSlot) {
        return this.startTime.compareTo(timeSlot.startTime);
    }
}
