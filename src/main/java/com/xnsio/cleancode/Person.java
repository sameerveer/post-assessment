package com.xnsio.cleancode;

import java.time.LocalDate;
import java.util.Map;
import java.util.TreeSet;

public class Person {
    private final String name;
    private final Map<LocalDate, TreeSet<TimeSlot>> calender;

    public Person(String name, Map<LocalDate, TreeSet<TimeSlot>> calender) {
        this.name = name;
        this.calender = calender;
    }

    public Map<LocalDate, TreeSet<TimeSlot>> getCalender() {
        return calender;
    }
}
