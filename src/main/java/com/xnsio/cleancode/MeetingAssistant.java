package com.xnsio.cleancode;

import java.time.LocalDate;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class MeetingAssistant {

    public TimeSlot findFirstAvailableTimeSlotFor(Person owner, Person attendee, LocalDate day) {
        final TreeSet<TimeSlot> ownersAvailableTimeSlots = findAvailableTimeSlots(owner, day);
        final TreeSet<TimeSlot> attendeesAvailableTimeSlots = findAvailableTimeSlots(attendee, day);
        for (TimeSlot timeSlot : ownersAvailableTimeSlots) {
            if (attendeesAvailableTimeSlots.contains(timeSlot)) {
                return timeSlot;
            }
        }
        return null;
    }

    private TreeSet<TimeSlot> findAvailableTimeSlots(Person person, LocalDate day) {
        final TreeSet<TimeSlot> timeSlots = person.getCalender().get(day);
        return timeSlots.stream()
                .filter(timeSlot -> timeSlot.isAvailable())
                .collect(Collectors.toCollection(TreeSet::new));

    }
}
