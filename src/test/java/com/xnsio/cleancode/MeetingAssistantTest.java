package com.xnsio.cleancode;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

public class MeetingAssistantTest {
    private MeetingAssistant meetingAssistant;
    private final LocalDate MEETING_DAY = getDateFor(2019, Month.NOVEMBER, 4);

    @Before
    public void setUp() throws Exception {
        meetingAssistant = new MeetingAssistant();
    }

    @Test
    public void ownersFirstTimeSlotAvailableForBothAttendees() {
        Person mini = new Person("Mini", getMinisCalender());
        Person james = new Person("James", getJamestsCalender());

        final TimeSlot expectedTimeSlot = getTimeSlot(2019, Month.NOVEMBER, 4, 13, 14, true, "");
        final TimeSlot availableTimeSlot = meetingAssistant.findFirstAvailableTimeSlotFor(mini, james, MEETING_DAY);
        Assert.assertEquals(expectedTimeSlot, availableTimeSlot);
    }

    @Test
    public void invitedAttendeesFirstTimeSlotAvailableForBothAttendees() {
        Person james = new Person("James", getJamestsCalender());
        Person frank = new Person("Frank", getFranksCalender());

        final TimeSlot expectedTimeSlot = getTimeSlot(2019, Month.NOVEMBER, 4, 12, 13, true, "");
        final TimeSlot availableTimeSlot = meetingAssistant.findFirstAvailableTimeSlotFor(frank, james, MEETING_DAY);
        Assert.assertEquals(expectedTimeSlot, availableTimeSlot);
    }

    @Test
    public void timeSlotNotAvailableForBothAttendees() {
        Person mini = new Person("Mini", getMinisCalender());
        Person frank = new Person("Frank", getFranksCalender());

        final TimeSlot availableTimeSlot = meetingAssistant.findFirstAvailableTimeSlotFor(mini, frank, MEETING_DAY);
        Assert.assertNull(availableTimeSlot);
    }

    private Map<LocalDate, TreeSet<TimeSlot>> getJamestsCalender() {
        TreeSet<TimeSlot> timeSlots = new TreeSet<>();
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 8, 9, false, "BUSY"));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 9, 10, false, "BUSY"));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 10, 11, false, "BUSY"));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 11, 12, false, "BUSY"));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 12, 13, true, ""));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 13, 14, true, ""));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 14, 15, false, "BUSY"));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 15, 16, true, ""));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 16, 17, false, "BUSY"));

        return getCalenderFor(MEETING_DAY, timeSlots);
    }

    private Map<LocalDate, TreeSet<TimeSlot>> getMinisCalender() {
        TreeSet<TimeSlot> timeSlots = new TreeSet<>();
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 8, 9, false, "BUSY"));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 9, 10, false, "BUSY"));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 10, 11, false, "BUSY"));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 11, 12, false, "BUSY"));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 12, 13, false, "BUSY"));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 13, 14, true, ""));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 14, 15, true, ""));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 15, 16, false, "BUSY"));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 16, 17, true, "BUSY"));

        return getCalenderFor(MEETING_DAY, timeSlots);
    }

    private Map<LocalDate, TreeSet<TimeSlot>> getFranksCalender() {
        TreeSet<TimeSlot> timeSlots = new TreeSet<>();
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 8, 9, true, ""));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 9, 10, true, ""));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 10, 11, true, ""));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 11, 12, true, ""));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 12, 13, true, ""));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 13, 14, false, "BUSY"));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 14, 15, false, "BUSY"));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 15, 16, true, ""));
        timeSlots.add(getTimeSlot(2019, Month.NOVEMBER, 4, 16, 17, false, "BUSY"));

        return getCalenderFor(MEETING_DAY, timeSlots);
    }

    private Map<LocalDate, TreeSet<TimeSlot>> getCalenderFor(LocalDate day, TreeSet<TimeSlot> timeSlots) {
        Map<LocalDate, TreeSet<TimeSlot>> calender = new TreeMap<>();
        calender.put(day, timeSlots);
        return calender;
    }

    private LocalDate getDateFor(int year, Month month, int day) {
        return LocalDate.of(year, month, day);
    }

    private TimeSlot getTimeSlot(int year, Month month, int day, int startTime, int endTime, boolean available, String title) {
        return new TimeSlot(LocalDateTime.of(2019, Month.NOVEMBER, day, startTime, 0),
                LocalDateTime.of(2019, Month.NOVEMBER, day, endTime, 0), available, title);
    }
}